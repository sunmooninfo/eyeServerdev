# 点击进入付费知识平台

# 点击获取毕业论文指导

# 点击获取就业解密培训

# 点击获取创业看护服务

# 点击获取加盟合作方式

# eyeServerdev

eyeServerdev为付费知识平台“毕业论文通式（毕业通）+就业武装通式（就业通）+创业配套通式（创业通）”业务的后端仓库

点击进入Gitee、GitHub开放仓库地址[![Gitee stars](https://gitee.com/sunmooninfo/eyeServerdev/badge/star.svg)](https://gitee.com/sunmooninfo/eyeServerdev)[![GitHub stars](https://img.shields.io/github/stars/sunmooninfo/eyeServerdev.svg?style=social&label=Stars)](https://github.com/sunmooninfo/eyeServerdev)

eyeVuedev为付费知识平台“毕业论文通式（毕业通）+就业武装通式（就业通）+创业配套通式（创业通）”业务的Vue端仓库

点击进入Gitee、GitHub开放仓库地址[![Gitee stars](https://gitee.com/sunmooninfo/eyeVuedev/badge/star.svg)](https://gitee.com/sunmooninfo/eyeVuedev)[![GitHub stars](https://img.shields.io/github/stars/sunmooninfo/eyeVuedev.svg?style=social&label=Stars)](https://github.com/sunmooninfo/eyeVuedev)


eyeUnidev为付费知识平台“毕业论文通式（毕业通）+就业武装通式（就业通）+创业配套通式（创业通）”业务的Uni跨终端前端页面仓库

点击进入Gitee、GitHub开放仓库地址[![Gitee stars](https://gitee.com/sunmooninfo/eyeUnidev/badge/star.svg)](https://gitee.com/sunmooninfo/eyeUnidev)[![GitHub stars](https://img.shields.io/github/stars/sunmooninfo/eyeUnidev.svg?style=social&label=Stars)](https://github.com/sunmooninfo/eyeUnidev)

`eye`不仅仅是一套拿来即用的完整产品项目，而且还是一套具备 **基础框架** - **前端模板** - **后端模板** - **系统监测** - **分布式架构** - **自动化部署**- **持续集成** - **无缝升级** - **开放仓库**  - **学习仓库** 的全方位企业级开发框架。

# 在线实例

## 预览图例

# 快速启动

# 正式启动

# 拆解准备

## 开发系统

## 开发工具

## 技术选型

### 前端技术选型

### 美工技术选型

### 后端技术选型

### 运维技术选型

# 从创建到完整
## 设计纲领

### 约定内容

### 代码设计图

### 数据库设计图

## 核心模块

## 其他模块

# 从完整到优化

# 从优化到扩展

* 拆解全部内容，附带扩展资料

# 开放许可

# 其他推荐内容





















* [文档](./doc/README.md)
* [贡献](./doc/CONTRIBUTE.md)
* [FAQ](./doc/FAQ.md)
* [API](./doc/api.md)

## 项目实例

该项目为前后端分离项目的后端部分，前端后台管理+付费知识平台PC端项目地址：[传送门](https://gitee.com/sunmooninfo/eyeVuedev);

付费知识平台多端开发项目(微信小程序端+app端)地址：[传送门](https://gitee.com/sunmooninfo/eyeUnidev)。

### 付费知识平台微信小程序实例

![](./doc/pics/readme/ulovemall_wx_demo.jpg)

### 付费知识平台移动app实例

下载链接 [下载链接](https://gitee.com/sunmooninfo/eyeUnidev)

### 付费知识平台移动浏览器实例

手机浏览器打开: https://wwwdev.6eye9.com/

### 管理后台实例

![](./doc/pics/readme/admin-dashboard.png)

1. 浏览器打开，输入以下网址: [https://wwwadmindev.6eye9.com/](https://wwwadmindev.6eye9.com/)

2. 用户访问用户名`guest`，管理员密码`Guest123`

> 注意：此实例是付费知识平台的管理后台，请勿删除和更改数据。

## 项目架构
![](./doc/pics/project/project-structure.jpg)

## 技术栈

> 1. Spring Boot
> 2. Uiniapp
> 3. Vue
> 4. 微信小程序

![](doc/pics/readme/readme-structure.jpg)

### 付费知识平台功能

* 首页
* 专题列表、专题详情
* 分类列表、分类详情
* 品牌列表、品牌详情
* 新品首发、人气推荐
* 优惠券列表、优惠券选择
* 团购、秒杀、会员
* 搜索
* 商品详情、商品评价、商品分享
* 购物车
* 下单
* 订单列表、订单详情、订单售后
* 地址、收藏、足迹、意见反馈
* 客服

### 管理平台功能

* 系统管理
* 文章管理
* 用户管理
* 商城管理
* 商品管理
* 统计报表
* 附带工具
* 外链

## [快速启动](./doc/deploy.md)

1. 配置最小开发环境：
    * [MySQL](./doc/deploy.md)
    * [JDK1.8或以上](./doc/deploy.md)
    * [Maven](./doc/deploy.md)
    * [Elasticsearch](./doc/deploy.md)
    * [Erlang](./doc/deploy.md)
    * [RabbitMQ](./doc/deploy.md)

### 安装OpenJDK

yum search java-11-openjdk

yum install java-11-openjdk-devel.x86_64

### 安装Maven

wget https://mirrors.tuna.tsinghua.edu.cn/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.zip

unzip apache-maven-3.6.3-bin.zip

vi /etc/profile

在最后添加

export M2_HOME=/root/apache-maven-3.6.3

export PATH=$PATH:$JAVA_HOME/bin:$M2_HOME/bin

source /etc/profile
    
    
2. 数据库依次导入eye-db/sql下的数据库文件
    * 创建数据库eye
    * eye_table.sql
    * eye_data.sql

3. 启动付费知识平台和管理后台及其各个网站的后端服务

    打开命令行，输入以下命令
    ```bash
    cd eyeServer
    mvn 
    mvn clean package
    启动项目：
    nohup java -Dfile.encoding=UTF-8 -jar eye-all/target/eye-all-0.1.0-exec.jar
    查看日志：
    tail -f -n5000 nohup.out
    ```
    
4. 启动管理后台前端

    打开命令行，输入以下命令
    ```bash
    npm install -g cnpm --registry=https://registry.npm.taobao.org
    cd eye/eye-admin
    cnpm install
    cnpm run dev
    ```
    此时，浏览器打开，输入网址`http://localhost:9527`, 此时进入管理后台登录页面。
    
5. 启动知识付费平台微信小程序前端

6. 打包知识付费平台移动app前端

7. 打包知识付费平台移动H5前端

8. 启动品牌网前端

9. 启动文章网前端


## 问题

![](doc/pics/readme/qq.png)

 * 开发者有问题或者好的建议可以用Issues反馈交流，请给出详细信息
 * 在开发交流群中应讨论开发、业务和合作问题
 * 如果真的需要QQ群里提问，请在提问前先完成以下过程：
    * 请仔细阅读本项目文档，特别是是[**FAQ**](./doc/FAQ.md)，查看能否解决；
    * 请阅读[提问的智慧](https://github.com/ryanhanwu/How-To-Ask-Questions-The-Smart-Way/blob/master/README-zh_CN.md)；
    * 请百度或谷歌相关技术；
    * 请查看相关技术的官方文档，例如微信小程序的官方文档；
    * 请提问前尽可能做一些DEBUG或者思考分析，然后提问时给出详细的错误相关信息以及个人对问题的理解。


