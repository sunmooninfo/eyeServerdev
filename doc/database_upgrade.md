# 数据库更新操作规范


## 操作环境

此规范是为了扩展功能时修改数据库，客户升级功能时为了客户的数据不受影响而做的规范。

## 操作适应者

该规范是针对正在开发功能对数据有权限修改的开发者。

## 操作流程

#### 对数据库做新增表

- 通常情况下对于一个数据库做新增操作，使用可视化的Mysql连接工具Navicat，打开Navicat,双击打开要增加数据表的的数据库

![](./pics/database-upgrade/001.png)

- 单机`新建表`按钮，在弹出的对话框中输入`字段名称`、`字段类型`、`字段长度`、`小数点长度`、`是否为null`,并在下面填写默认值和注释，例如下图：

![](./pics/database-upgrade/002.png)

- 点击保存按钮，进行保存，会弹出`输入表名`的对话框，输入表名，点击`确定`,

![](./pics/database-upgrade/003.png)

- 关闭`新建表`对话框，可以看到在数据库中新增了一个刚刚新建的表`eye_test`,右击 --> 选择对象信息，点击DDL，会看到刚刚创建表的sql语句，复制到要升级的sql脚本中,并使用if exits 语句判断是否存在该表，有则创建，没有则跳过,在sql最前方添加$$$执行下面语句。

修改后sql语句：
```
 $$$ CREATE TABLE IF NOT EXISTS `eye_test` (
   `id` int NOT NULL,
   `name` varchar(255) DEFAULT NULL,
   `password` varchar(255) DEFAULT NULL,
   `sex` tinyint(1) DEFAULT '0' COMMENT '性别 1:男 2：女',
   PRIMARY KEY (`id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci; 
```

#### 对数据库中的表新增字段

- 右击要新增字段的数据表，选择`设计表`

![](./pics/database-upgrade/005.png)

- 点击`添加栏位`或者`插入栏位`，在合适的位置插入字段，填写`字段名称`、`字段类型`、`字段长度`、`小数点长度`、`是否为null`，例如在eye_test表中添加`deleted`逻辑删除字段

![](./pics/database-upgrade/006.png)

- 点击`SQL预览`按钮，复制sql语句到更新的sql脚本中,并利用存储过程判断是否存在该列，没有插入，有则跳过。

修改后slq语句：
```
DROP PROCEDURE IF EXISTS deleted;$$$
CREATE PROCEDURE deleted() BEGIN
IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name='eye_test' AND COLUMN_NAME='deleted') THEN
ALTER TABLE `eye_test`
ADD COLUMN `deleted`  tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除 1：删除 0：未删除' AFTER `sex`;
END IF;
END;$$$
CALL deleted;$$$
DROP PROCEDURE deleted;$$$
```

#### 对数据库中的字段修改、删除与新增的操作一致。

```修改sql语句
DROP PROCEDURE IF EXISTS gender;$$$
CREATE PROCEDURE gender() BEGIN
IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name='eye_test' AND COLUMN_NAME='gender') THEN
ALTER TABLE `eye_test`
CHANGE COLUMN `sex` `gender`  tinyint(1) NULL DEFAULT 0 COMMENT '性别 1:男 2：女' AFTER `password`;
END IF;
END;$$$
CALL gender;$$$
DROP PROCEDURE gender;$$$
```

```删除sql语句
DROP PROCEDURE IF EXISTS deleted;$$$
CREATE PROCEDURE deleted() BEGIN
IF EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name='eye_test' AND COLUMN_NAME='deleted') THEN
ALTER TABLE `eye_test`
DROP COLUMN `deleted`;
END IF;
END;$$$
CALL deleted;$$$
DROP PROCEDURE deleted;$$$
```
## 说明

为了防止升级对之前的版本的不支持，数据库在大版本升级前禁止对正式的数据库进行`删除`、`修改`操作。

# 更新

## 代码更新

拉取仓库最新代码，修改必要配置项后，编译运行即可（若开发运行环境有变更，需配置部署对应开发运行环境）。

## 数据库更新

### 初次安装

#### 创建数据库

#### 导入数据库结构（eye_table.sql）

#### 导入数据库必要演示数据（eye_data.sql，各项10条左右）

### 补丁更新

#### 代码备份，数据库备份，开发运行环境备份（记录使用到组件的版本号或进行服务器快照备份），防止更新失败后无法恢复

#### 创建数据库并导入结构与数据后，伴随着必要的代码补丁更新，数据库也需要一起进行补丁更新（执行eye_update.sql）

#### eye_upgrade.sql：默认为eye_table.sql的数据库补丁更新，内含新增数据库、表、字段内容并复刻必要旧有数据，不进行改删操作（在一个大版本下，可能包含多个eye_upgrade.sql，须按照时间顺序依次执行。若在补丁更新中发生错误，可查阅相关文档或联系开发人员进行操作）

### 版本更新（针对老用户）

#### 代码备份，数据库备份，开发运行环境备份，防止更新失败后无法恢复

#### 随着代码补丁与数据库补丁的增加，从各种优化角度来说，必须进行大版本更新（执行eye_upgrade.sql）

#### eye_upgrade.sql：默认为eye_table.sql的数据库版本更新，除包含新增数据库、表、字段内容并复刻必要旧有数据外，还包含改删等危险操作（新增内容为之前全部补丁的合并，改操作会将旧数据保留到新库、表、字段中，删操作会自动删除系统默认为不使用的库、表、字段，若意外改删了用户所使用到的库、表、字段，可查阅相关文档或联系开发人员进行数据恢复）