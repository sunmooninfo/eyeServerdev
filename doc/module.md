# eyeServerdev

    eye-admin-api:后台管理模块 端口号: 8091
    eye-information-api:个人信息模块 端口号: 8092
    eye-brand-api:品牌管理模块 端口号: 8094
    eye-cms-api:文章管理模块   端口号: 8095
    eye-common-api:通用的模块  端口号: 8090
    eye-core: 核心模块
    eye-db:公共模块
    eye-express:物流模块
    eye-maill:邮件服务模块
    eye-search:搜索模块
    eye-sms:短信服务模块
    eye-storage:文件存储模块
    eye-tool-api:附带工具模块  端口号: 9099
    eye-all:启动全部模块       端口号: 8060
    eye-eureka-server:Eureka Server服务 端口号: 8761
    eye-seckill-apr:秒杀模块   端口号: 8050
    
