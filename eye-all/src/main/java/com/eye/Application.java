package com.eye;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages = "com.eye")
@MapperScan(value= {"com.eye.db"})
//@ComponentScan(basePackages = {"com.eye","com.eye.admin","com.eye.brand","com.eye.admin","com.eye.cms","com.eye.db","com.eye.common","com.eye.core","com.eye.storage","com.eye.sms","com.eye.express","com.eye.mail"}, excludeFilters = {
//                @ComponentScan.Filter(type = FilterType.REGEX,pattern = "com.search")
//        } )
@EnableTransactionManagement
@EnableScheduling
public class Application {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

}