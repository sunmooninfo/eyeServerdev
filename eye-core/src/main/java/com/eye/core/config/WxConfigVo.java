package com.eye.core.config;

public class WxConfigVo {

    private String vipNotifyUrl;

    public WxConfigVo() {
    }

    public WxConfigVo(String vipNotifyUrl) {
        this.vipNotifyUrl = vipNotifyUrl;
    }

    public String getVipNotifyUrl() {
        return vipNotifyUrl;
    }

    public void setVipNotifyUrl(String vipNotifyUrl) {
        this.vipNotifyUrl = vipNotifyUrl;
    }

    @Override
    public String toString() {
        return "WxConfigVo{" +
                "vipNotifyUrl='" + vipNotifyUrl + '\'' +
                '}';
    }
}
