/*
MySQL Backup
Source Server Version: 8.0.20
Source Database: eye
Date: 2020/12/17 10:49:54
*/


-- ----------------------------
--  新增字段'kill——number'
-- ----------------------------
DROP PROCEDURE IF EXISTS kill_number;
CREATE PROCEDURE kill_number() BEGIN
IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name='eye_cart' AND COLUMN_NAME='kill_number') THEN
ALTER TABLE `eye_cart`
ADD COLUMN  `kill_number`  smallint NULL DEFAULT 0 COMMENT '秒杀商品数量' AFTER `number`;
END IF;
END;
CALL kill_number;
DROP PROCEDURE kill_number;

-- ----------------------------
--  新增字段'goods_kill_id'
-- ----------------------------
DROP PROCEDURE IF EXISTS goods_kill_id;
CREATE PROCEDURE goods_kill_id() BEGIN
IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name='eye_cart' AND COLUMN_NAME='goods_kill_id') THEN
ALTER TABLE `eye_cart`
ADD COLUMN `goods_kill_id`  int NULL DEFAULT NULL COMMENT '秒杀商品ID' AFTER `goods_name`;
END IF;
END;
CALL goods_kill_id;
DROP PROCEDURE goods_kill_id;