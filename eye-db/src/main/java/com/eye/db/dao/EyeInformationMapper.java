package com.eye.db.dao;

import com.eye.db.domain.EyeInformation;
import com.eye.db.domain.EyeInformationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EyeInformationMapper {
    long countByExample(EyeInformationExample example);

    int deleteByExample(EyeInformationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(EyeInformation record);

    int insertSelective(EyeInformation record);

    EyeInformation selectOneByExample(EyeInformationExample example);

    EyeInformation selectOneByExampleSelective(@Param("example") EyeInformationExample example, @Param("selective") EyeInformation.Column ... selective);

    List<EyeInformation> selectByExampleSelective(@Param("example") EyeInformationExample example, @Param("selective") EyeInformation.Column ... selective);

    List<EyeInformation> selectByExample(EyeInformationExample example);

    EyeInformation selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") EyeInformation.Column ... selective);

    EyeInformation selectByPrimaryKey(Integer id);

    EyeInformation selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    int updateByExampleSelective(@Param("record") EyeInformation record, @Param("example") EyeInformationExample example);

    int updateByExample(@Param("record") EyeInformation record, @Param("example") EyeInformationExample example);

    int updateByPrimaryKeySelective(EyeInformation record);

    int updateByPrimaryKey(EyeInformation record);

    int logicalDeleteByExample(@Param("example") EyeInformationExample example);

    int logicalDeleteByPrimaryKey(Integer id);
}