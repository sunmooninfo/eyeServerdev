package com.eye.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer//spring启动时启动Eureka Server服务
public class EyeEurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(EyeEurekaServerApplication.class,args);
    }
}
