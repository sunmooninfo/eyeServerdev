package com.eye.information;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages = {"com.eye.db","com.eye.information","com.eye.core","com.eye.storage"})
@MapperScan({"com.eye.db"})
@EnableTransactionManagement
@EnableScheduling
public class EyeInformationApplication {

    public static void main(String[] args) {
        SpringApplication.run(EyeInformationApplication.class,args);
    }
}
