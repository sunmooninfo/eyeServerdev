package com.eye.information.service;

import java.time.LocalDateTime;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.eye.db.dao.EyeInformationMapper;
import com.eye.db.domain.EyeInformation;
import com.eye.db.domain.EyeInformationExample;
import com.eye.db.domain.EyeToolAccount;
import com.eye.db.domain.EyeToolAccountExample;
import com.github.pagehelper.PageHelper;

@Service
public class EyeInformationService extends EyeToolAccount{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Resource
	private EyeInformationMapper informationMapper;	


	//查询数据
	public List<EyeInformation> querySelective(Integer page, Integer limit, String sort, String order) {
		EyeInformationExample example = new EyeInformationExample();
		EyeInformationExample.Criteria criteria = example.createCriteria();
		criteria.andDeletedEqualTo(false);//查询未删除的数据
		//根据添加时间进行排序（升序）
		if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
			example.setOrderByClause("add_time ASC,"+sort + " " + order);
		}
		PageHelper.startPage(page, limit);
		return informationMapper.selectByExample(example);
	}

	//添加数据
	public void add(EyeInformation information) {
		information.setAddTime(LocalDateTime.now());
		information.setUpdateTime(LocalDateTime.now());
		informationMapper.insertSelective(information);
	}

	public void deleteById(Integer id) {
		informationMapper.logicalDeleteByPrimaryKey(id);
		
	}

}
