package com.eye.information.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.eye.core.utils.ResponseUtil;
import com.eye.core.validator.Order;
import com.eye.core.validator.Sort;
import com.eye.db.domain.EyeInformation;
import com.eye.information.service.EyeInformationService;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 个人信息模块Controller
 */

@RestController
@RequestMapping("/admin/information")//前端url写死了一个admin，所以这里也要加admin
@Api(description = "个人信息模块")
@Validated
public class EyeInformationController {


	private final Log logger = LogFactory.getLog(EyeInformationController.class);

	@Autowired
	private EyeInformationService eyeInformationService;

	/**
	 * 查询数据
	 * @param page   页码设置
	 * @param limit  一页显示内容设置
	 * @param sort   排序字段设置
	 * @param order  排序规则设置
	 * @return
	 */
	@ApiOperation(value = "个人信息列表查询")
	//新模块，暂时功能较少，不适合权限设置，暂时注解掉，后期根据增加功能再做设置。	
	//@RequiresPermissions("admin:information:list")
	//@RequiresPermissionsDesc(menu = {"表单", "信息"}, button = "查询")
	@GetMapping("/list")
	public Object list( 
			@RequestParam(defaultValue = "1") Integer page,
			@RequestParam(defaultValue = "10") Integer limit,
			@Sort @RequestParam(defaultValue = "add_time") String sort,
			@Order @RequestParam(defaultValue = "desc") String order) {
		List<EyeInformation> personalList=eyeInformationService.querySelective(page, limit, sort, order);
		return ResponseUtil.okList(personalList);
	}

	/**
	 * 添加跟人信息数据
	 * @param information
	 * @return
	 */
	@ApiOperation(value = "添加个人信息数据")
	//@RequiresPermissions("admin:information:create")
	//@RequiresPermissionsDesc(menu = {"表单", "信息"}, button = "添加")
	@PostMapping("/create")
	public Object create(@RequestBody EyeInformation information) {
		eyeInformationService.add(information);
		return ResponseUtil.ok();
	}

	@ApiOperation(value = "个人信息删除")
	//@RequiresPermissions("admin:information:delete")
	//@RequiresPermissionsDesc(menu = {"表单", "信息"}, button = "删除")
	@PostMapping("/delete")
	public Object delete(@RequestBody EyeInformation information) {
		Integer id = information.getId();
		if (id == null) {
			return ResponseUtil.badArgument();
		}
		eyeInformationService.deleteById(id);
		return ResponseUtil.ok();
	}

}
