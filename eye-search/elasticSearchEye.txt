DELETE /eye
PUT /eye
{
  "settings": {
    "number_of_shards": 2
    , "number_of_replicas": 1
  }
}
POST /eye/ulove_article/_mapping
{
  "_source": {
    "excludes":["context"]
  }, 
 	"properties": {
   	  "id":{
   	    "type": "integer",
   	    "index":false
   	  },
      "title": {
          "type": "text",
          "analyzer":"ik_max_word",
          "search_analyzer":"ik_smart"
      },
       "label": {
          "type": "text",
          "analyzer":"ik_max_word",
          "search_analyzer":"ik_smart"
      },
       "categoryName": {
          "type": "text",
          "analyzer":"ik_max_word",
          "search_analyzer":"ik_smart"
      },
       "picUrl":{
		   "type":"text",
		   "index":false
	    },
      "context": {
          "type": "text",
          "analyzer":"ik_max_word",
          "search_analyzer":"ik_smart"
       },
        "sortOrder":{
		   "type":"short",
		   "index":false
	    },
	      "link":{
		   "type":"text",
		   "index":false
	    },
       "addTime": {
      		"type":   "date",
      		"format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd"
    	 }
  }
}
