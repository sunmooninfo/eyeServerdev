#全文搜索引擎

#新增cms搜索功能
##接口定义：/cms/search/list

##参数说明：
 
 | 含义 | 必填 | 参数名 | 类型|默认值|
 | ------ | ------ | ------ |------|------|
 | 请求方式|  |  |get||
 | 用户查询关键字 | 是 | query |String||
 | 查询页数 | 否 | page |Integer|1|
 | 一页显示行数 | 否 | limit|Integer|10|
 | 排序字段 | 否 | sort| String |addTime|
 | 排序规则 | 否 | order| String |desc|
 
 ###请求参数格式：
 http://localhost:8060/cms/search/list?query=无缝
 
 ###返回参数：
    {
      "errno": 0,
      "data": {
        "total": 1,
        "pages": 1,
        "limit": 10,
        "page": 1,
        "list": [
          {
            "id": 28,
            "title": "双相S32750<em style='color:red'>不锈钢</em>管,无缝钢管",
            "label": "",
            "categoryName": "双相不锈钢管",
            "sortOrder": 2,
            "link": "/productDetail?id=",
            "addTime": "2020-10-10 13:51:59"
          }
        ]
      },
      "errmsg": "成功"
    }
     

 

