package com.search.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

public class EyeSearchArticle implements Serializable {

    private Integer id;
    private String title;
    private String label;
    private String categoryName;
    private String picUrl;
    private String context;
    private short sortOrder;
    private String link;
    private String addTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public short getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(short sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public EyeSearchArticle(Integer id, String title, String label, String categoryName, String picUrl, String context, short sortOrder, String link, String addTime) {
        this.id = id;
        this.title = title;
        this.label = label;
        this.categoryName = categoryName;
        this.picUrl = picUrl;
        this.context = context;
        this.sortOrder = sortOrder;
        this.link = link;
        this.addTime = addTime;
    }

    public EyeSearchArticle() {
    }

    @Override
    public String toString() {
        return "EyeSearchArticle{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", label='" + label + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", picUrl='" + picUrl + '\'' +
                ", context='" + context + '\'' +
                ", sortOrder=" + sortOrder +
                ", link='" + link + '\'' +
                ", addTime='" + addTime + '\'' +
                '}';
    }
}
