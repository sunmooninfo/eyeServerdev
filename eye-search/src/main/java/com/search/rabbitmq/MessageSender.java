package com.search.rabbitmq;

import com.eye.core.utils.ESConstant;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageSender {

    @Autowired
    private AmqpTemplate amqpTemplate;

    public void updateIndex(String IndexName){

        amqpTemplate.convertAndSend(ESConstant.exchange,ESConstant.update_index_rk,IndexName);
    }

}