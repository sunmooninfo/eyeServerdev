package com.search.service;


import com.eye.core.utils.ESConstant;
import com.eye.core.utils.ResponseUtil;
import com.eye.db.dao.EyeArticleMapper;
import com.eye.db.domain.EyeArticle;
import com.eye.db.domain.EyeArticleExample;
import com.eye.db.domain.EyeCategory;
import com.eye.db.service.EyeCategoryService;
import com.eye.db.util.JsonUtils;
import com.eye.db.util.SearchUtil;
import com.search.dto.EyeSearchArticle;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.IndicesClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EyeSearchService {
    @Autowired
    private RestHighLevelClient restHighLevelClient;
    @Autowired
    private EyeArticleMapper eyeArticleMapper;
    @Autowired
    private EyeCategoryService eyeCategoryService;
    public Object importAll() {
        try {
            EyeArticleExample example = new EyeArticleExample();
            example.or().andIsSearchEqualTo(true).andDeletedEqualTo(false);
            List<EyeArticle> uloveArticles = eyeArticleMapper.selectByExample(example);
            BulkRequest bulkRequest = new BulkRequest();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            for(EyeArticle uloveArticle :uloveArticles){
                EyeSearchArticle uloveSearchArticle = new EyeSearchArticle();
                uloveSearchArticle.setId(uloveArticle.getId());
                uloveSearchArticle.setTitle(uloveArticle.getTitle());
                uloveSearchArticle.setLabel(uloveArticle.getLabel());
                EyeCategory uloveArticleCategory = eyeCategoryService.findById(uloveArticle.getCategoryId());
                uloveSearchArticle.setCategoryName(uloveArticleCategory.getName());
                String formatTime = dtf.format(uloveArticle.getAddTime());
                uloveSearchArticle.setAddTime(formatTime);
                uloveSearchArticle.setPicUrl(uloveArticle.getPicUrl());
                uloveSearchArticle.setContext(uloveArticle.getContext());
                uloveSearchArticle.setLink(uloveArticle.getLink());
                uloveSearchArticle.setSortOrder(uloveArticle.getSortOrder());

                //向对象转化成json格式
                String esArticle = JsonUtils.objectToJson(uloveSearchArticle);
                //将数据导入到ElasticSearch中
                IndexRequest indexRequest = new IndexRequest("index", "ulove_article");
                indexRequest.id(uloveArticle.getId().toString());
                indexRequest.source(esArticle, XContentType.JSON);
                bulkRequest.add(indexRequest);
            }
            BulkResponse bulk = this.restHighLevelClient.bulk(bulkRequest);
            String s = bulk.buildFailureMessage();

            if (bulk.hasFailures()) { // 只要有一个操作失败了，这个方法就返回 true\
                for (BulkItemResponse bulkItemResponse : bulk) {
                    if (bulkItemResponse.isFailed()) { //Indicate if a given operation failed
                        BulkItemResponse.Failure failure = bulkItemResponse.getFailure(); //Retrieve the failure of the failed operation
                        String message = failure.getMessage();
                        System.out.println("错误信息"+message);
                    }
                }
            }
            System.out.println("整体错误"+s);
            System.out.println("响应结果"+bulk);
            return ResponseUtil.ok();
        }catch (Exception e){
            e.printStackTrace();
        }
        return  ResponseUtil.fail(900,"导入数据失败");

    }

    /**
     * 从ElasticSearch中搜索数据
     * @param query
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @return
     */
    public Object searchAll(String query, Integer page, Integer limit, String sort, String order) {
        try {
            SearchRequest searchRequest = new SearchRequest("index").types("ulove_article");
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            searchSourceBuilder.query(QueryBuilders.multiMatchQuery(query, new String[]{"title", "label", "categoryName", "context"}));
            searchSourceBuilder.from(page - 1);
            searchSourceBuilder.size(limit);
            if(order.equals("asc")){
                searchSourceBuilder.sort(sort, SortOrder.ASC);
            }else{
                searchSourceBuilder.sort(sort, SortOrder.DESC);
            }
            //设置高亮
            HighlightBuilder highlightBuilder = new HighlightBuilder();
            highlightBuilder.preTags("<em style='color:red'>");
            highlightBuilder.postTags("</em>");
            highlightBuilder.fields().add(new HighlightBuilder.Field("title"));
            searchSourceBuilder.highlighter(highlightBuilder);
            searchRequest.source(searchSourceBuilder);
            SearchResponse searchResponse = this.restHighLevelClient.search(searchRequest);
            if (searchResponse == null) {
                return null;
            }
            SearchHits hits = searchResponse.getHits();
            SearchHit[] hits1 = hits.getHits();
            List<EyeSearchArticle> esDocumentList = new ArrayList<>();
            for (int i = 0; i < hits1.length; i++) {
                SearchHit documentFields = hits1[i];
                String sourceAsString = documentFields.getSourceAsString();
                EyeSearchArticle esDocument = JsonUtils.jsonToPojo(sourceAsString, EyeSearchArticle.class);
                Map<String, HighlightField> highlightFields = documentFields.getHighlightFields();
                HighlightField highLightField = highlightFields.get("title");
                if (highLightField != null) {
                    Text[] fragments = highLightField.fragments();
                    String highlightDocuments = StringUtils.join(fragments);
                    esDocument.setTitle(highlightDocuments);
                    esDocumentList.add(esDocument);

                } else {
                    esDocumentList.add(esDocument);
                }
            }
            HashMap<String, Object> resultMap = new HashMap<>();
            resultMap.put("total",hits.getTotalHits());
            resultMap.put("queryResult",esDocumentList);
            resultMap.put("categoryId",1036030);
            resultMap.put("page",page);
            resultMap.put("limit",limit);
            resultMap.put("pages",(int)esDocumentList.size()/limit+1);
            return ResponseUtil.ok(resultMap);
        }catch (Exception e){
            e.printStackTrace();
        }

    return ResponseUtil.fail(SearchUtil.SEARCH_FAIL,"搜索失败");
    }

    public Object createIndex(String indexName,String typeName) throws IOException {
        //创建索引请求对象，并设置索引名称
        CreateIndexRequest createIndexRequest = new CreateIndexRequest(indexName);
        //设置索引参数
        createIndexRequest.settings(Settings.builder().put("number_of_shards",2).put("number_of_replicas",1));
        //创建映射
        createIndexRequest.mapping(typeName, "{\n" +
                "  \"_source\": {\n" +
                "    \"excludes\":[\"context\"]\n" +
                "  }, \n" +
                " \t\"properties\": {\n" +
                "   \t  \"id\":{\n" +
                "   \t    \"type\": \"integer\",\n" +
                "   \t    \"index\":false\n" +
                "   \t  },\n" +
                "      \"title\": {\n" +
                "          \"type\": \"text\",\n" +
                "          \"analyzer\":\"ik_max_word\",\n" +
                "          \"search_analyzer\":\"ik_smart\"\n" +
                "      },\n" +
                "       \"label\": {\n" +
                "          \"type\": \"text\",\n" +
                "          \"analyzer\":\"ik_max_word\",\n" +
                "          \"search_analyzer\":\"ik_smart\"\n" +
                "      },\n" +
                "       \"categoryName\": {\n" +
                "          \"type\": \"text\",\n" +
                "          \"analyzer\":\"ik_max_word\",\n" +
                "          \"search_analyzer\":\"ik_smart\"\n" +
                "      },\n" +
                "       \"picUrl\":{\n" +
                "\t\t   \"type\":\"text\",\n" +
                "\t\t   \"index\":false\n" +
                "\t    },\n" +
                "      \"context\": {\n" +
                "          \"type\": \"text\",\n" +
                "          \"analyzer\":\"ik_max_word\",\n" +
                "          \"search_analyzer\":\"ik_smart\"\n" +
                "       },\n" +
                "        \"sortOrder\":{\n" +
                "\t\t   \"type\":\"short\",\n" +
                "\t\t   \"index\":false\n" +
                "\t    },\n" +
                "\t      \"link\":{\n" +
                "\t\t   \"type\":\"text\",\n" +
                "\t\t   \"index\":false\n" +
                "\t    },\n" +
                "       \"addTime\": {\n" +
                "      \t\t\"type\":   \"date\",\n" +
                "      \t\t\"format\": \"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd\"\n" +
                "    \t }\n" +
                "  }\n" +
                "}\n", XContentType.JSON);
        //创建索引操作客户端
        IndicesClient indices = restHighLevelClient.indices();
        //创建响应对象
        CreateIndexResponse createIndexResponse = indices.create(createIndexRequest);
        //得到响应结果
        boolean acknowledged = createIndexResponse.isAcknowledged();
        System.out.println("创建索引结果：" + acknowledged);
        if(acknowledged){
            this.importAll();
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail(SearchUtil.CREATE_ESINDEX_FAIL,"创建ElasticSearch索引失败");
    }

    public Object updateIndex(String indexName) throws IOException {
        //创建删除索引请求对象
        DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest(indexName);
        //创建索引操作客户端
        IndicesClient indices = restHighLevelClient.indices();
        //删除索引
        DeleteIndexResponse deleteResponse = indices.delete(deleteIndexRequest);
        //判断删除响应结果
        boolean acknowledged = deleteResponse.isAcknowledged();
        System.out.println("删除索引结果：" + acknowledged);
        if(acknowledged){
            this.createIndex(ESConstant.indexName,ESConstant.typeName);
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail(SearchUtil.DELETE_ESINDEX_FAIL,"删除ElasticSearch索引失败");
    }
}
