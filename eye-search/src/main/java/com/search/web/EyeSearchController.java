package com.search.web;

import com.eye.core.utils.ESConstant;
import com.eye.core.utils.ResponseUtil;
import com.eye.core.validator.Order;
import com.eye.core.validator.Sort;
import com.eye.db.util.SearchUtil;
import com.search.service.EyeSearchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * CMS搜索服务Controller
 */

@RestController
@RequestMapping("/cms/search")
@Api(description = "搜索服务")
public class EyeSearchController {

    private final Log logger = LogFactory.getLog(EyeSearchController.class);

    @Autowired
    private EyeSearchService eyeSearchService;

    /**
     * 从ElasticSearch中查询数据
     */
    @GetMapping("list")
    @ApiOperation("根据关键字搜索产品")
    @ApiImplicitParams({
            @ApiImplicitParam(name="query",value = "查询关键字",required=true,paramType="path",dataType="String"),
            @ApiImplicitParam(name="page",value = "显示页数",required=false,paramType="path",dataType="Integer"),
            @ApiImplicitParam(name="limit",value = "显示行数",required=false,paramType="path",dataType="Integer"),
            @ApiImplicitParam(name="sort",value = "排序字段",required=false,paramType="path",dataType="String"),
            @ApiImplicitParam(name="order",value = "排序规则",required=false,paramType="path",dataType="String")
    })
    public Object searchAll(String query,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort(accepts = {"addTime"}) @RequestParam(defaultValue = "addTime") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order){
        String info = "根据关键字搜索产品";
        logger.info(getClass()+"----------------------------"+info);
        return eyeSearchService.searchAll(query,page,limit,sort,order);
    }

    /**
     *删除ElasticSearch索引
     */

    @GetMapping("updateIndex")
    @ApiOperation("更新ES索引")
    @ApiImplicitParam(name="indexName",value = "删除ES索引名称",required=true,paramType="path",dataType="String")
    @RabbitListener(
            bindings = @QueueBinding(
                    value = @Queue(value = ESConstant.update_queue,autoDelete = "false"),
                    exchange = @Exchange(value = ESConstant.exchange,type = ExchangeTypes.TOPIC,autoDelete = "false"),
                    key = ESConstant.update_index_rk
            )
    )
    public  Object updateIndex(String indexName){
        try {
            String info = "更新ES索引";
            logger.info(getClass()+"----------------------------"+info);
            return eyeSearchService.updateIndex(indexName);
        }catch (Exception e){
            e.printStackTrace();
        }
        return ResponseUtil.fail(SearchUtil.DELETE_ESINDEX_FAIL,"删除ElasticSearch索引失败");
    }


}
